import {
  REGISTER_FETCHING,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
} from "../constants";

import axios from "axios";

export const setMypageStateToFetch = () => ({
  type: REGISTER_FETCHING,
});

export const setMypageStateToSuccess = (payload) => ({
  type: REGISTER_SUCCESS,
  payload,
});

export const setMypageStateToFailed = (payload) => ({
  type: REGISTER_FAILED,
  payload,
});

export const register = (account) => {
  return async (dispatch) => {
    try {
      dispatch(setMypageStateToFetch());
      const result = await axios.post(
        "http://localhost:8081/api/v2/login",
        account
      );
      dispatch(setMypageStateToSuccess(result.data));
    } catch (e) {
      dispatch(setMypageStateToFailed({ error: JSON.stringify(e) }));
    }
  };
};
