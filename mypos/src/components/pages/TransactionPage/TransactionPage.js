import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as transactionActions from "./../../../actions/transaction.action";
import { makeStyles } from "@material-ui/core/styles";
import Moment from "react-moment";
import NumberFormat from "react-number-format";
import { Typography, Grid } from "@material-ui/core";
import MaterialTable from "material-table";
import { imageUrl } from "./../../../constants";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    marginTop: 55,
  },
  star: {
    color: "red",
  },
  orderList: {
    overflowX: "hidden",
    height: 490,
    flex: 1,
    width: "100%",
    maxHeight: 490,
  },
  orderListItem: {
    height: 100,
    maxHeight: 100,
  },
  productContainer: {
    height: 720,
  },
  paymentButton: {
    height: 95,
  },
}));

export default (props) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [selectedRow, setSelectedRow] = useState(null);
  const [orderList, setorderList] = useState([]);

  const transactionReducer = useSelector(
    ({ transactionReducer }) => transactionReducer
  );

  useEffect(() => {
    dispatch(transactionActions.getTransactions());
  }, []);

  const transactionColumns = [
    {
      title: "ID",
      field: "transaction_id",
    },

    {
      title: "DATE",
      render: (item) => <Moment format="YYYY/MM/DD">{item.timestamp}</Moment>,
    },
    {
      title: "TIME",
      render: (item) => <Moment format="HH:mm">{item.timestamp}</Moment>,
    },
    {
      title: "STAFF",
      field: "staff_id",
    },
    {
      title: "TOTAL",
      render: (item) => (
        <NumberFormat
          value={item.total}
          displayType={"text"}
          thousandSeparator={true}
          prefix={"฿"}
        />
      ),
    },
    {
      title: "PAID",
      render: (item) => (
        <NumberFormat
          value={item.paid}
          displayType={"text"}
          thousandSeparator={true}
          prefix={"฿"}
        />
      ),
    },
    {
      title: "#SKU",
      render: (item) => (
        <Typography>{JSON.parse(item.order_list).length} SKU</Typography>
      ),
    },
    {
      title: "WAY",
      field: "payment_type",
    },
  ];

  const orderColumns = [
    {
      title: "ID",
      field: "product_id",
      render: (item) => (
        <Typography variant="body1">{item.product_id}</Typography>
      ),
    },
    {
      title: "IMAGE",
      field: "image",
      cellStyle: { padding: 0 },
      render: (item) => (
        <img
          src={`${imageUrl}/images/${item.image}?dummy=${Math.random()}`}
          style={{ width: 80, height: 80, borderRadius: "5%" }}
        />
      ),
    },
    {
      title: "NAME",
      cellStyle: { minWidth: 400 },
      field: "name",
      render: (item) => <Typography variant="body1">{item.name}</Typography>,
    },
    {
      title: "PRICE",
      field: "price",
      render: (item) => (
        <Typography variant="body1">
          <NumberFormat
            value={item.price}
            displayType={"text"}
            thousandSeparator={true}
            decimalScale={2}
            fixedDecimalScale={true}
            prefix={"฿"}
          />
        </Typography>
      ),
    },
    {
      title: "STOCK",
      field: "stock",
      render: (item) => (
        <Typography variant="body1">
          <NumberFormat
            value={item.stock}
            displayType={"text"}
            thousandSeparator={true}
            decimalScale={0}
            fixedDecimalScale={true}
            suffix={" pcs"}
          />
        </Typography>
      ),
    },
  ];

  const clickTransactionRow = (data) => {
    setSelectedRow(data);
    setorderList(JSON.parse(data.order_list));
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={7}>
        <Grid item xs={6}>
          <MaterialTable
            id="root_stock"
            title="Stock"
            columns={transactionColumns}
            data={transactionReducer.result ? transactionReducer.result : []}
            onRowClick={(evt, data) => clickTransactionRow(data)}
            options={{
              pageSize: 10,
              rowStyle: (rowData) => ({
                backgroundColor:
                  selectedRow &&
                  selectedRow.tableData.id === rowData.tableData.id
                    ? "#EEE"
                    : "#FFF",
              }),
            }}
          />
        </Grid>

        <Grid item xs={6}>
          <MaterialTable
            title="Order List"
            columns={orderColumns}
            data={orderList}
            options={{
              search: false,
              paging: false,
            }}
          />
        </Grid>
      </Grid>
    </div>
  );
};

// import React, { useEffect, useState } from "react";
// import { useSelector, useDispatch } from "react-redux";
// import { makeStyles } from "@material-ui/core/styles";
// import Moment from "react-moment";
// import NumberFormat from "react-number-format";
// import MaterialTable, { MTableToolbar } from "material-table";
// import Button from "@material-ui/core/Button";
// import Dialog from "@material-ui/core/Dialog";
// import DialogActions from "@material-ui/core/DialogActions";
// import DialogContent from "@material-ui/core/DialogContent";
// import DialogContentText from "@material-ui/core/DialogContentText";
// import { Typography, Grid } from "@material-ui/core";
// import StockCard from "./../../fragments/StockCard/StockCard";
// import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
// import NewReleasesIcon from "@material-ui/icons/NewReleases";
// import AssignmentReturnIcon from "@material-ui/icons/AssignmentReturn";
// import StarIcon from "@material-ui/icons/Star";
// import * as TransAction from "../../../actions/transaction.action";

// const useStyles = makeStyles((theme) => ({
//   root: {
//     width: "100%",
//   },
// }));
// export default (props) => {
//   const classes = useStyles();
//   const transactionReducer = useSelector(
//     ({ transactionReducer }) => transactionReducer
//   );
//   const dispatch = useDispatch();

//   const [selectedRow, setSelectedRow] = useState(null);
//   const [openDialog, setOpenDialog] = useState(false);
//   const [selectedProduct, setSelectedProduct] = useState(null);

//   useEffect(() => {
//     dispatch(TransAction.getTransactions());
//   }, []);

//   const columns = [
//     {
//       title: "ID",
//       field: "product_id",
//       render: (item) => (
//         <Typography variant="body1">{item.transaction_id}</Typography>
//       ),
//     },
//     {
//       title: "PRICE",
//       field: "price",
//       render: (item) => (
//         <Typography variant="body1">
//           <NumberFormat
//             value={item.total}
//             displayType={"text"}
//             thousandSeparator={true}
//             decimalScale={2}
//             fixedDecimalScale={true}
//             prefix={"฿"}
//           />
//         </Typography>
//       ),
//     },
//     {
//       title: "ODER_LIST",
//       field: "order_list",
//       render: (item) => (
//         <Typography variant="body1">
//           {JSON.parse(item.order_list)
//             .map((a) => a.name)
//             .join(",")}
//           {/* "[{"name": "dsads", "id": "0"}, {"name": "dsads", "id": "0"}, {"name": "dsads", "id": "0"}]" item.order_list     :::string (JSON)*/}

//           {/* [{"name": "dsads", "id": "0"}, {"name": "dsads", "id": "0"}, {"name": "dsads", "id": "0"}]  JSON.parse(item.order_list) :::array(object)*/}

//           {/* ["dsads", "dsads", "dsads"]   JSON.parse(item.order_list).map(a => a.name)      :::array[string]*/}

//           {/* "dsads, dsads, dsads"  JSON.parse(item.order_list).map(a => a.name).join(",")    ::: string*/}
//         </Typography>
//       ),
//     },
//     {
//       title: "CREATED",
//       field: "created",
//       render: (item) => (
//         <Typography>
//           <Moment format="DD/MM/YYYY">{item.created}</Moment>
//         </Typography>
//       ),
//     },
//   ];

//   const handleClose = () => {
//     setOpenDialog(false);
//   };

//   const showDialog = () => {
//     if (selectedProduct === null) {
//       return "";
//     }

//     return (
//       <Dialog
//         open={openDialog}
//         keepMounted
//         onClose={() => {}}
//         aria-labelledby="alert-dialog-slide-title"
//         aria-describedby="alert-dialog-slide-description"
//       >
//         <DialogContent>
//           <DialogContentText id="alert-dialog-slide-description">
//             You cannot restore deleted product.
//           </DialogContentText>
//         </DialogContent>
//       </Dialog>
//     );
//   };

//   return (
//     <div className={classes.root}>
//       {/* Summary Icons */}
//       <MaterialTable
//         id="root_stock"
//         title="Stock"
//         columns={columns}
//         data={transactionReducer.result ? transactionReducer.result : []}
//         onRowClick={(evt, selectedRow) => setSelectedRow(selectedRow)}
//         options={{
//           pageSize: 5,
//           rowStyle: (rowData) => ({
//             backgroundColor:
//               selectedRow && selectedRow.tableData.id === rowData.tableData.id
//                 ? "#EEE"
//                 : "#FFF",
//           }),
//         }}
//       />

//       {showDialog()}
//     </div>
//   );
// };
