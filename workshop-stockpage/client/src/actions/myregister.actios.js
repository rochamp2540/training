import {
  MYREGISTER_FAILED,
  MYREGISTER_FETCHING,
  MYREGISTER_SUCCESS,
} from "../constants";

export const setRegisterStateToFetch = (payload) => ({
  type: MYREGISTER_FETCHING,
  payload,
});

export const setRegisterStateToSuccess = (payload) => ({
  type: MYREGISTER_SUCCESS,
  payload,
});

export const setRegisterStateToFaild = (payload) => ({
  type: MYREGISTER_FAILED,
  payload,
});

export const register = (account) => {
  return async (dispatch) => {
    try {
      dispatch(setRegisterStateToFetch());
      // setSubmitting(true);
      setTimeout(function () {
        console.log(account);
        dispatch(setRegisterStateToSuccess(account));

        //   var message = [];
        //   if (values.username === "admin") {
        //     message.push("aaaaa");
        //   }
        //   if (values.password === values.conpassword) {
        //     message.push("bbbbb");
        //   }
        //   if (message.length > 0) {
        //     alert(message.join("\n"));
        //   }
        // setSubmitting(false);
      }, 1000);
    } catch (e) {
      dispatch(setRegisterStateToFaild());
    }
  };
};
