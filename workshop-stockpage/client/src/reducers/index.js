import { combineReducers } from "redux";
import registerReducer from "./register.reducer";
import myregisterReducer from "./myregister.reducer";
import mypageReducer from "./mypage.reducer";
import loginReducer from "./login.reducer";
import stockReducer from "./stock.reducer";
import shopReducer from "./shop.reducer";
import transactionReducer from "./transaction.reducer";
import reportReducer from "./report.reducer";

export default combineReducers({
  registerReducer,
  myregisterReducer,
  mypageReducer,
  loginReducer,
  stockReducer,
  shopReducer,
  transactionReducer,
  reportReducer,
});
