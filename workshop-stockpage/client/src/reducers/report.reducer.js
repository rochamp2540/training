import { REPORT_SUCCESS, REPORT_FETCHING, REPORT_FAILED } from "../constants";

const initialState = {
  result: [],
  isFetching: false,
  isError: false,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case REPORT_FETCHING:
      return { ...state, result: [], isFetching: true, isError: false };
    case REPORT_SUCCESS:
      return { ...state, result: payload, isFetching: false, isError: false };
    case REPORT_FAILED:
      return { ...state, result: [], isFetching: false, isError: true };
    default:
      return state;
  }
};
