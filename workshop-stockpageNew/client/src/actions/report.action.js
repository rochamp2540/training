import { httpClient } from "./../utils/HttpClient";
import {
  REPORT_SUCCESS,
  REPORT_FETCHING,
  REPORT_FAILED,
  server,
} from "../constants";

export const setStateReportToSuccess = (payload) => ({
  type: REPORT_SUCCESS,
  payload,
});

const setStateReportToFetching = () => ({
  type: REPORT_FETCHING,
});

const setStateReportToFailed = () => ({
  type: REPORT_FAILED,
});

export const getProducts = () => {
  return (dispatch) => {
    dispatch(setStateReportToFetching());
    doGetProducts(dispatch);
  };
};

const doGetProducts = async (dispatch) => {
  try {
    let result = await httpClient.get(server.REPORT_URL);
    dispatch(setStateReportToSuccess(result.data));
  } catch (err) {
    // alert(JSON.stringify(err));
    dispatch(setStateReportToFailed());
  }
};
