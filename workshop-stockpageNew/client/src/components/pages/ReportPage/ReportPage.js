import React, { useEffect, useState } from "react";
import { Line, Bar, Pie } from "react-chartjs-2";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Button, ButtonGroup, IconButton } from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import RefreshIcon from "@material-ui/icons/Refresh";
import * as reportAction from "../../../actions/report.action";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: "white",
    padding: 24,
  },
}));

export default () => {
  const classes = useStyles();
  const reportReducer = useSelector(({ reportReducer }) => reportReducer);
  const dispatch = useDispatch();
  const [chartType, setChartType] = useState("bar");
  const allType = ["Bicycle", "Bikecycle", "Helicopter", "Airplane", "SpaceShip"];
  // const [chartData, setChartData] = useState([]);

  const getResult = () => {
    let result = reportReducer.result;
    let currentVal = 0;
    let result2 = [];
    allType.map((type) => {
      result.map((data) => {
        if (data._id == type) {
          currentVal = data.countItem;
        }
      });
      result2.push(currentVal);
      currentVal = 0;
    });
    return result2;
  };

  useEffect(() => {
    dispatch(reportAction.getProducts());
  }, []);

  const data = {
    labels: allType,
    datasets: [
      {
        label: "Revenue 2021",
        fill: true,
        lineTension: 0.1,
        backgroundColor: [
          "rgba(255, 99, 132, 0.5)",
          "rgba(54, 162, 235, 0.5)",
          "rgba(255, 206, 86, 0.5)",
          "rgba(75, 192, 192, 0.5)",
          "rgba(153, 102, 255, 0.5)",
          "rgba(255, 159, 64, 0.5)",
        ],
        borderColor: "rgba(0,192,192,1)",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgba(75,192,192,1)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(75,192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: getResult(),
      },
    ],
  };

  const chartOption = {
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            callback: function (value, index, values) {
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            },
          },
        },
      ],
    },
  };

  return (
    <div className={classes.root}>
      <Typography variant="h1">Stock Chart</Typography>
      <ButtonGroup
        size="large"
        color="primary"
        aria-label="large outlined primary button group"
      >
        <Button
          variant={chartType === "line" ? "contained" : "outlined"}
          onClick={() => setChartType("line")}
        >
          Line
        </Button>
        <Button
          variant={chartType === "bar" ? "contained" : "outlined"}
          onClick={() => setChartType("bar")}
        >
          Bar
        </Button>
        <Button
          variant={chartType === "pie" ? "contained" : "outlined"}
          onClick={() => setChartType("pie")}
        >
          Pie
        </Button>
      </ButtonGroup>

      <div style={{ height: 500 }}>
        {chartType === "line" && (
          <Line data={data} width="100%" height={50} options={chartOption} />
        )}
        {chartType === "pie" && (
          <Pie data={data} width="100%" height={50} options={chartOption} />
        )}
        {chartType === "bar" && (
          <Bar data={data} width="100%" height={50} options={chartOption} />
        )}
      </div>
      {/* {console.log(`aaaa ${reportReducer.result}`)} */}
    </div>
  );
};
