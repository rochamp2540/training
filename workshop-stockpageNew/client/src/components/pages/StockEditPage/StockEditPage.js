import * as stockActions from "./../../../actions/stock.action";

import React, { useEffect } from "react";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { imageUrl } from "./../../../constants";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";

import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

import { TextField } from "formik-material-ui";

import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import { Select } from "formik-material-ui";
import PhotoCamera from "@material-ui/icons/PhotoCamera";

import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import FormControll from "react-bootstrap/FormControl";
import FormGroup from "react-bootstrap/FormGroup";
import FormLabel from "react-bootstrap/FormLabel";

import { Label } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "80%",
    marginTop: 100,
  },
  field: {
    marginTop: 16,
  },
  card: {
    padding: 20,
    border: "box-shadow: 0 4px 8px 0 rgba(1, 1, 1, 0.5)",
  },
}));

export default (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const stockReducer = useSelector(({ stockReducer }) => stockReducer);

  const testSchema = Yup.object().shape({
    name: Yup.string().required("Enter Name"),
    price: Yup.string().required("Enter Price"),
    stock: Yup.string().required("Enter Stock"),
    types: Yup.string().required("Enter Types"),
  });

  useEffect(() => {
    let id = props.match.params.id;
    dispatch(stockActions.getProductById(id));
  }, []);

  const showForm = ({ values, setFieldValue, errors }) => {
    return (
      <Form>
        <Card className={classes.card}>
          <CardContent>
            <Typography gutterBottom variant="h3">
              Edit Product
            </Typography>

            <Field
              className={classes.field}
              fullWidth
              component={() => <h3>Product ID# {values.product_id}</h3>}
              name="product_id"
              type="text"
              label="Id"
            />
            <br />

            <Field
              className={classes.field}
              fullWidth
              component={TextField}
              name="name"
              type="text"
              label="Name"
            />
            <br />
            <Field
              className={classes.field}
              fullWidth
              component={TextField}
              name="price"
              type="number"
              label="Price"
            />

            <Field
              className={classes.field}
              fullWidth
              component={TextField}
              name="stock"
              type="number"
              label="Stock"
            />

            <FormControl>
              {/* <InputLabel htmlFor="age-simple">Types</InputLabel> */}
              <Field
                // component={Select}
                as="select"
                name="types"
                inputProps={{
                  id: "age-simple",
                }}
              >
                <option value={"Bicycle"}>Bicycle</option>
                <option value={"Bikecycle"}>Bikecycle</option>
                <option value={"Helicopter"}>Helicopter</option>
                <option value={"Airplane"}>Airplane</option>
                <option value={"SpaceShip"}>SpaceShip</option>
              </Field>
              <p className="MuiFormHelperText-root Mui-error MuiFormHelperText-filled">
                {errors.types}
              </p>
            </FormControl>

            <div className={classes.field}>{showPreviewImage(values)}</div>

            <div className={classes.field}>
              {/* <img
                src={`${process.env.PUBLIC_URL}/images/ic_photo.png`}
                style={{ width: 25, height: 20 }}
              /> */}
              {/* <span style={{ color: "#00B0CD", marginLeft: 10 }}>
                Add Picture
              </span> */}
              <Button
                variant="contained"
                color="primary"
                component="label"
                startIcon={<PhotoCamera />}
              >
                Upload
                <input
                  type="file"
                  onChange={(e) => {
                    e.preventDefault();
                    setFieldValue("file", e.target.files[0]); // for upload
                    setFieldValue(
                      "file_obj",
                      URL.createObjectURL(e.target.files[0])
                    ); // for preview image
                  }}
                  name="image"
                  click-type="type1"
                  className="picupload"
                  multiple
                  accept="image/*"
                  id="files"
                  style={{ padding: "20px 0" }}
                  hidden // hidden default
                />
              </Button>
            </div>
          </CardContent>

          <CardActions>
            <Button variant="contained" color="secondary" type="submit">
              Edit
            </Button>
            <Button
              component={Link}
              to="/stock"
              variant="contained"
              style={{ background: "red", color: "white" }}
            >
              Cancel
            </Button>
          </CardActions>
        </Card>
      </Form>
    );
  };

  const showForm2 = ({ values, setFieldValue, errors, touched }) => {
    return (
      <Form>
        <Typography gutterBottom variant="h3">
          Edit Product
        </Typography>
        <div className="form-group">
          <label htmlFor="firstName" style={errors.name && { color: "red" }}>
            Name
          </label>
          <Field
            // component={TextField}
            name="name"
            type="text"
            className={"form-control"}
            style={errors.name && { borderColor: "red" }} //{{borderColor: (errors.name ?  "red" : "initial") }}
          />
        </div>
        <p style={{ color: "red" }}>{errors.name}</p>
        <div className="form-group">
          <label htmlFor="lastName" style={errors.price && { color: "red" }}>
            Price
          </label>
          <Field
            name="price"
            type="number"
            className={"form-control"}
            style={errors.price && { borderColor: "red" }}
          />
        </div>
        <p style={{ color: "red" }}>{errors.price}</p>
        <div className="form-group">
          <label htmlFor="email" style={errors.stock && { color: "red" }}>
            Stock
          </label>
          <Field
            name="stock"
            type="number"
            className={"form-control"}
            style={errors.stock && { borderColor: "red" }}
          />
        </div>
        <p style={{ color: "red" }}>{errors.stock}</p>

        <div className="form-group">
          {/* <InputLabel htmlFor="age-simple">Types</InputLabel> */}
          <FormControl>
            <Field
              as="select"
              name="types"
              inputProps={{
                id: "age-simple",
              }}
              className="form-control"
            >
              <option value={"Bicycle"}>Bicycle</option>
              <option value={"Bikecycle"}>Bikecycle</option>
              <option value={"Helicopter"}>Helicopter</option>
              <option value={"Airplane"}>Airplane</option>
              <option value={"SpaceShip"}>SpaceShip</option>
            </Field>
            <p style={{ color: "red" }}>{errors.types}</p>
          </FormControl>
        </div>

        <div>{showPreviewImage(values)}</div>
        <div className="form-group">
          <Button
            variant="contained"
            color="primary"
            component="label"
            startIcon={<PhotoCamera />}
          >
            Upload
            <input
              type="file"
              onChange={(e) => {
                e.preventDefault();
                if (e.target.files[0]) {
                  setFieldValue("file", e.target.files[0]); // for upload
                  setFieldValue(
                    "file_obj",
                    URL.createObjectURL(e.target.files[0])
                  ); // for preview image
                } else {
                  setFieldValue("file", null); // for upload
                  setFieldValue("file_obj", null); // for preview image
                }
              }}
              name="image"
              click-type="type1"
              className="picupload"
              multiple
              accept="image/*"
              id="files"
              style={{ padding: "20px 0" }}
              hidden // hidden default
            />
          </Button>
        </div>
        <div className="form-group">
          <button type="submit" className="btn btn-primary mr-2">
            Edit
          </button>
          <Button
            component={Link}
            to="/stock"
            variant="contained"
            style={{ background: "red", color: "white" }}
          >
            Cancel
          </Button>
        </div>
      </Form>
    );
  };

  const showPreviewImage = (values) => {
    if (values.file_obj) {
      return <img src={values.file_obj} style={{ height: 100 }} />;
    } else if (values.image) {
      return (
        <img
          src={`${imageUrl}/images/${values.image}`}
          style={{ height: 100 }}
        />
      );
    }
  };

  return (
    <Container className={classes.root}>
      {console.log(stockReducer.result)}
      {/* Main content */}
      <div className="box-body" style={{ marginTop: 30 }}>
        <Formik
          validationSchema={testSchema}
          enableReinitialize
          validate={(values) => {
            let errors = {};
            // if (!values.name) errors.name = "Enter name";
            // if (!values.stock) errors.stock = "Enter stock";
            // if (!values.price) errors.price = "Enter price";
            return errors;
          }}
          initialValues={
            stockReducer.result
              ? stockReducer.result
              : { name: "loading", price: 0, stock: 0, types: "" }
          }
          onSubmit={(values, { setSubmitting }) => {
            let formData = new FormData();
            formData.append("product_id", values.product_id);
            formData.append("name", values.name);
            formData.append("price", values.price);
            formData.append("stock", values.stock);
            formData.append("types", values.types);
            if (values.file) {
              formData.append("image", values.file);
            }
            dispatch(stockActions.updateProduct(formData, props.history));
            setSubmitting(false);
          }}
        >
          {(props) => showForm2(props)}
        </Formik>
      </div>
      {/* /.content */}
    </Container>
  );
};
