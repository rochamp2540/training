import * as stockActions from "./../../../actions/stock.action";

import { useState } from "react";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

import { TextField } from "formik-material-ui";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
// import Select from "@material-ui/core/Select";
import { Select } from "formik-material-ui";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import OutdoorGrillIcon from "@material-ui/icons/OutdoorGrill";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "80%",
    marginTop: 0,
  },
  field: {
    marginTop: 16,
  },
  card: {
    padding: 20,
    boxShadow: "0 4px 8px 0 rgba(0,0,0,0.5)",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [types, setTypes] = useState("");

  const testSchema = Yup.object().shape({
    // name: Yup.string().required("Enter Name"),
    // price: Yup.string().required("Enter Price"),
    // stock: Yup.string().required("Enter Stock"),
    // types: Yup.string().required("Enter Types"),
    // image: Yup.mixed().required("Enter Image"),
  });

  const handleChange = (event) => {
    setTypes(event.target.value);
  };

  const showPreviewImage = (values) => {
    if (values.file_obj) {
      return <img src={values.file_obj} style={{ height: 100 }} />;
    }
  };

  const showForm = ({ values, setFieldValue, errors }) => {
    return (
      <Form>
        <Card className={classes.card}>
          <CardContent>
            <Typography gutterBottom variant="h3">
              Create Stock
            </Typography>
            <Field
              className={classes.field}
              fullWidth
              component={TextField}
              name="name"
              type="text"
              label="Name"
            />
            <br />
            <Field
              className={classes.field}
              fullWidth
              component={TextField}
              name="price"
              type="number"
              label="Price"
            />
            <Field
              className={classes.field}
              fullWidth
              component={TextField}
              name="stock"
              type="number"
              label="Stock"
            />

            <FormControl>
              <InputLabel htmlFor="age-simple">Types</InputLabel>
              <Field
                component={Select}
                name="types"
                inputProps={{
                  id: "age-simple",
                }}
              >
                <MenuItem value={"Bicycle"}>Bicycle</MenuItem>
                <MenuItem value={"Bikecycle"}>Bikecycle</MenuItem>
                <MenuItem value={"Helicopter"}>Helicopter</MenuItem>
                <MenuItem value={"Airplane"}>Airplane</MenuItem>
                <MenuItem value={"SpaceShip"}>SpaceShip</MenuItem>
              </Field>
              <p className="MuiFormHelperText-root Mui-error MuiFormHelperText-filled">
                {errors.types}
              </p>
            </FormControl>

            {/* <FormControl className={classes.formControl,classes.field}>
              <InputLabel id="demo-simple-select-label">Types</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={types}
                onChange={handleChange}
              >
                <MenuItem value={"Bicycle"}>Bicycle</MenuItem>
                <MenuItem value={"Bikecycle"}>Bikecycle</MenuItem>
                <MenuItem value={"Helicopter"}>Helicopter</MenuItem>
                <MenuItem value={"Airplane"}>Airplane</MenuItem>
                <MenuItem value={"SpaceShip"}>SpaceShip</MenuItem>
              </Select>
            </FormControl> */}
            <div>{showPreviewImage(values)}</div>
            <div className={classes.field}>
              {/* <img
                src={`${process.env.PUBLIC_URL}/images/ic_photo.png`}
                style={{ width: 25, height: 20 }}
              />
              <span style={{ color: "#00B0CD", marginLeft: 10 }}>
                Add Picture
              </span> */}
              <Button
                variant="contained"
                color="primary"
                component="label"
                startIcon={<PhotoCamera />}
              >
                Upload
                <input
                  type="file"
                  onChange={(e) => {
                    e.preventDefault();
                    if (e.target.files[0]) {
                      setFieldValue("file", e.target.files[0]); // for upload
                      setFieldValue(
                        "file_obj",
                        URL.createObjectURL(e.target.files[0])
                      ); // for preview image
                    } else {
                      setFieldValue("file", null); // for upload
                      setFieldValue("file_obj", null); // for preview image
                    }
                  }}
                  name="image"
                  click-type="type1"
                  className="picupload"
                  multiple
                  accept="image/*"
                  id="files"
                  style={{ padding: "20px 0" }}
                  hidden // hidden default
                />
              </Button>
            </div>
          </CardContent>

          <CardActions>
            <Button variant="contained" color="secondary" type="submit">
              Create
            </Button>

            <Button
              component={Link}
              to="/stock"
              variant="contained"
              style={{ background: "red", color: "white" }}
            >
              Cancel
            </Button>
          </CardActions>
        </Card>
      </Form>
    );
  };

  const showForm2 = ({ values, setFieldValue, errors, touched }) => {
    return (
      <Form>
        <Typography gutterBottom variant="h3">
          Create Stock
        </Typography>
        <div className="form-group">
          <label htmlFor="firstName">Name</label>
          <Field
            // component={TextField}
            name="name"
            type="text"
            className={"form-control"}
            placeholder="Name"
            // style={errors.name && { borderColor: "red" }} //{{borderColor: (errors.name ?  "red" : "initial") }}
          />
        </div>
        <ErrorMessage name="name">
          {(msg) => <p style={{ color: "red" }}>{msg}</p>}
        </ErrorMessage>
        {/* <p style={{ color: "red" }}>{errors.name}</p> */}

        <div className="form-group">
          <label htmlFor="lastName">Price</label>
          <Field
            name="price"
            type="number"
            className={"form-control"}
            // style={errors.price && { borderColor: "red" }}
          />
        </div>
        <ErrorMessage name="price">
          {(msg) => <p style={{ color: "red" }}>{msg}</p>}
        </ErrorMessage>
        {/* <p style={{ color: "red" }}>{errors.price}</p> */}

        <div className="form-group">
          <label htmlFor="email">Stock</label>
          <Field
            name="stock"
            type="number"
            className={"form-control"}
            // style={errors.stock && { borderColor: "red" }}
          />
        </div>
        <ErrorMessage name="stock">
          {(msg) => <p style={{ color: "red" }}>{msg}</p>}
        </ErrorMessage>
        {/* <p style={{ color: "red" }}>{errors.stock}</p> */}

        <div className="form-group">
          <FormControl>
            <InputLabel htmlFor="age-simple">Types</InputLabel>
            <Field
              component={Select}
              name="types"
              inputProps={{
                id: "age-simple",
              }}
            >
              <MenuItem value={"Bicycle"}>Bicycle</MenuItem>
              <MenuItem value={"Bikecycle"}>Bikecycle</MenuItem>
              <MenuItem value={"Helicopter"}>Helicopter</MenuItem>
              <MenuItem value={"Airplane"}>Airplane</MenuItem>
              <MenuItem value={"SpaceShip"}>SpaceShip</MenuItem>
            </Field>
            <ErrorMessage name="types">
              {(msg) => <p style={{ color: "red" }}>{msg}</p>}
            </ErrorMessage>
            {/* <p style={{ color: "red" }}>{errors.types}</p> */}
          </FormControl>
        </div>
        <div>{showPreviewImage(values)}</div>
        <div className="form-group">
          <Button
            variant="contained"
            color="primary"
            component="label"
            startIcon={<PhotoCamera />}
          >
            Upload
            <input
              type="file"
              onChange={(e) => {
                e.preventDefault();
                if (e.target.files[0]) {
                  // setImage(false);
                  setFieldValue("file", e.target.files[0]); // for upload
                  setFieldValue(
                    "file_obj",
                    URL.createObjectURL(e.target.files[0])
                  ); // for preview image
                } else {
                  // setImage(true);
                  setFieldValue("file", null); // for upload
                  setFieldValue("file_obj", null); // for preview image
                }
              }}
              name="image"
              click-type="type1"
              className="picupload"
              multiple
              accept="image/*"
              id="files"
              style={{ padding: "20px 0" }}
              hidden // hidden default
            />
          </Button>
          <ErrorMessage name="image">
            {(msg) => <p style={{ color: "red" }}>{msg}</p>}
          </ErrorMessage>
          {/* <p style={{ color: "red" }}>{errors.image}</p> */}
        </div>
        <div className="form-group">
          <button type="submit" className="btn btn-primary mr-2">
            Create
          </button>
          <Button
            component={Link}
            to="/stock"
            variant="contained"
            style={{ background: "red", color: "white" }}
          >
            Cancel
          </Button>
        </div>
      </Form>
    );
  };

  return (
    <Container className={classes.root}>
      {/* Main content */}

      <div className="box-body" style={{ marginTop: 30 }}>
        <Formik
          validationSchema={testSchema}
          validate={(values) => {
            let errors = {};
            if (!values.name) errors.name = "Enter name";
            // if (!values.stock) errors.stock = "Enter stock";
            // if (!values.price) errors.price = "Enter price";
            if (!values.types) errors.types = "Enter types";
            if (!values.file) errors.image = "Enter Image";
            return errors;
          }}
          initialValues={{ name: "", stock: 0, price: 0, types: "", image: "" }}
          onSubmit={(values, { setSubmitting }) => {
            let formData = new FormData();
            formData.append("name", values.name);
            formData.append("price", values.price);
            formData.append("stock", values.stock);
            formData.append("image", values.file);
            formData.append("types", values.types);
            dispatch(stockActions.addProduct(formData, props.history));
            setSubmitting(false);
          }}
        >
          {(props) => showForm2(props)}
        </Formik>
      </div>
      {/* /.content */}
    </Container>
  );
};
