import React from "react";
import { shallow } from "enzyme";
import AddProductPage from "./AddProductPage";

describe("AddProductPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<AddProductPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
