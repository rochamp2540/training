const express = require("express");
const router = express();
const jwt = require("./jwt");
const Products = require("./models/product_schema");

const formidable = require("formidable");
const path = require("path");
const fs = require("fs-extra");
const { route } = require("./api_product");

router.get("/report", async (req, res) => { //, jwt.verify,
  const products = await Products.aggregate([
    {
      $group: {
        _id: "$types",
        countItem: { $sum: 1 },
      },
    },
  ]);
  res.json(products);
});

module.exports = router;
