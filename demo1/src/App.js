import React, { useState } from "react";
import "./App.css";

export default function App() {
  const Header = () => {
    return (
      <div>
        <h1>wowwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww</h1>
      </div>
    );
  };
  //props => {count,setCount,resultMessage,btnTitle}
  const CountMe = (props) => {
    return (
      <div>
        <button
          onClick={(e) => {
            props.setCount(props.count + 1);
            console.log(props.count);
          }}
        >
          Click Me
        </button>
        <span>
          {` ${props.resultMessage}`} = {props.count}
        </span>
      </div>
    );
  };

  var count = 0;
  const [count2, setCount2] = React.useState(0);
  const [count3, setCount3] = useState(0);

  return (
    <div>
      <Header />
      {/* <button
        onClick={(e) => {
          count += 1;
          console.log(count);
        }}
      >
        click me
      </button> */}

      {/* <button
        onClick={(e) => {
          setCount2(count2 + 1);
          console.log(count2);
        }}
      >
        click me
      </button>
      <span>count 2 = {count2}</span> */}

      <CountMe
        btnTitle="ClickMe"
        resultMessage="Count2 is :"
        setCount={setCount2}
        count={count2}
      />

      <CountMe
        btnTitle="ClickMe"
        resultMessage="Count3 is :"
        setCount={setCount3}
        count={count3}
      />
    </div>
  );
}
